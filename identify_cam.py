from PIL import Image, ImageDraw
import face_recognition

aman_image = face_recognition.load_image_file("known/me.jpeg")
salman_image = face_recognition.load_image_file("known/salman.jpg")
kp_image = face_recognition.load_image_file("known/kapil.jpg")

amn_enc = face_recognition.face_encodings(aman_image)[0]
sl_enc = face_recognition.face_encodings(salman_image)[0]
kp_enc = face_recognition.face_encodings(kp_image)[0]

known_encodings = [
    amn_enc,
    sl_enc,
    kp_enc,
]

known_encodings_names = [
    "Amn",
    "salman",
    "kapil"
]
import cv2
webcam = cv2.VideoCapture(0)
while True:
    ret,tst_img = webcam.read()

    # tst_img = face_recognition.load_image_file("unknown/kgm.jpeg")
    face_locations = face_recognition.face_locations(tst_img)

    tst_img_enc = face_recognition.face_encodings(tst_img,face_locations)

    pil_image = Image.fromarray(tst_img)
    draw = ImageDraw.Draw(pil_image)

    for(top,right,bottom,left), face_encoding in zip(face_locations,tst_img_enc):
        match =  face_recognition.compare_faces(known_encodings,face_encoding)
        name="unknown"
        if True in match:
            index = match.index(True)
            name = known_encodings_names[index]
        print(name)
        w,h = draw.textsize(name)
        # draw.rectangle(((left,top),(right,bottom)),outline= (0,255,255))
        # draw.text((left+6,bottom-h-10),name,fill=(0,255,0))

        cv2.rectangle(tst_img,(left,top),(right,bottom),(255,0,0),2)
        tst_img = cv2.putText(tst_img, name, (left,bottom - h - 10), cv2.FONT_HERSHEY_SIMPLEX ,  
                   1, (255, 0, 0) , 2, cv2.LINE_AA) 

        # cv2.putText(tst_img,name,(left,bottom),1,cv2.FONT_HERSHEY_SIMPLEX ,(255,0,0),2,cv2.LINE_AA  )
    cv2.imshow("Frame",tst_img)
    if cv2.waitKey(10)==ord("q"):
        break
# pil_image.show()
webcam.release()
cv2.distroyAllWindows()