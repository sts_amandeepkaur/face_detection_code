import face_recognition
from PIL import Image, ImageDraw

kp = face_recognition.load_image_file("known/kapil.jpg")
me = face_recognition.load_image_file("known/me.jpeg")

kp_enc = face_recognition.face_encodings(kp)[0]
me_enc = face_recognition.face_encodings(me)[0]

known_encodings=[
    kp_enc,me_enc
]

known_names = [
    "Kapil Sharma","Aman"
]

test_img = face_recognition.load_image_file("unknown/kgm.jpeg")

#find faces
faces = face_recognition.face_locations(test_img)
enc_tst = face_recognition.face_encodings(test_img,faces)
 
pil_image = Image.fromarray(test_img)
draw = ImageDraw.Draw(pil_image)
c=10
#loop through faces
for (top,right,bottom,left), face_encoding in zip(faces,enc_tst):
    matches = face_recognition.compare_faces(known_encodings,face_encoding)
    name="unknown"
    if True in matches:
        index = matches.index(True)
        name = known_names[index]
    print(name)
    #draw box
    draw.rectangle(((left,top),(right,bottom)),outline=(255,255,0))
    text_width, text_height = draw.textsize(name)
    print(draw.textsize(name))
    #cut & save
    # face_image = test_img[top:bottom,left:right]
    # pil_im = Image.fromarray(face_image)
    # pil_im.show()
    # pil_im.save("unknown/{}.jpg".format(c))
    # c+=1
    draw.rectangle(((left,bottom - text_height - 10), (right, bottom)), fill=(255,255,0), outline=(255,255,0))
    draw.text((left + 6, bottom - 10 ), name, fill=(0,0,0))
    
pil_image.show()